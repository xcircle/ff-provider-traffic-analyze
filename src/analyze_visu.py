#!/usr/bin/env python3

# import datetime
import json
import matplotlib.pyplot as plt
from matplotlib.dates import MONDAY
from matplotlib.dates import WeekdayLocator, DateFormatter

data = json.loads(open("test.json", "r").read())
xList = []
yList = []

for node in data:
    if node == "node-05":
        for provider in data[node]:
            if provider == "60294":
                for dtime in data[node][provider]:
                    xList.append(datetime.datetime.strptime(dtime, '%Y%m%d_%H%M%S'))
                    yList.append(float(data[node][provider][dtime]))

# every monday
mondays = WeekdayLocator(MONDAY)

# every 3rd month
monthsFmt = DateFormatter("%d.%m.%Y - %H:%M")
fig, ax = plt.subplots()
ax.plot_date(xList, yList, '-')
ax.autoscale_view()
ax.grid(True)

fig.autofmt_xdate()

plt.show()
