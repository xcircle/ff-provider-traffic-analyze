#!/bin/bash

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-01.json http://01.nodes.freifunk-aachen.de:8232/

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-02.json http://02.nodes.freifunk-aachen.de:8232/

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-03.json http://03.nodes.freifunk-aachen.de:8232/

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-04.json http://04.nodes.freifunk-aachen.de:8232/

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-05.json http://05.nodes.freifunk-aachen.de:8232/

wget -O /path/to/save/provider-traffic/$(date +"%Y%m%d_%H%M%S")_providertraffic_node-06.json http://06.nodes.freifunk-aachen.de:8232/

7z a /path/to/save/provider-traffic/provider_traffic.7z /path/to/save/provider-traffic/*.json -sdel
