#!/usr/bin/env python3

import os
import re
import glob
import json
import datetime

# path to source data (json files)
DATA_DIR = "data/"

stat = {}
tmp_stat = {}
tmp = {}
first_dtime = None
last_dtime = None

#########

for file in sorted(glob.glob(DATA_DIR + "*.json")):
    fpath, fname = os.path.split(file)

    # extract date, time and node number from filename
    m = re.search("(\d+_\d+).*(node-\d+).json", fname)
    if not m:
        print("couldn't extract date and time - skip file <%s>" % fname)
        continue

    # node number
    node = m.group(2)
    if node not in tmp:
        tmp[node] = {}

    # convert date and time from filename to datetime struct
    dt_from_fname = m.group(1)
    dt_file = datetime.datetime.strptime(dt_from_fname, '%Y%m%d_%H%M%S')
    tmp[node]["t_new"] = int(dt_file.timestamp())

    # save date and time from the first file to build
    # later the start and end datetime of all used files
    if first_dtime is None:
        first_dtime = dt_from_fname

    # need this for the first run
    if "t_old" not in tmp[node]:
        tmp[node]["t_old"] = tmp[node]["t_new"]

    # create the time delta per node of the actual file
    # and the last used file to calculate the traffic per second
    dt = abs(tmp[node]["t_new"] - tmp[node]["t_old"])

    try:
        json_data = json.loads(open(file, "r").read())
    except:
        print("unable to load <%s>" % fname)
        continue

    for provider in json_data:
        traffic_byte_total = float(json_data[provider])

        # need this for the first run
        if provider not in tmp[node]:
            tmp[node][provider] = traffic_byte_total

        # traffic per second
        #
        # traffic_new - traffic_old
        # -------------------------
        #         t_delta
        traffic_byte_dt = traffic_byte_total - tmp[node][provider]

        if traffic_byte_dt < 0:
            traffic_byte_dt = traffic_byte_total

        if dt == 0:
            traffic_byte_sec = 0
        else:
            traffic_byte_sec = traffic_byte_dt / dt

        if node not in tmp_stat:
            tmp_stat[node] = {}

        if provider not in tmp_stat[node]:
            # tmp_stat[node][provider] = {tmp[node]["t_new"]: traffic_byte_sec}
            tmp_stat[node][provider] = {dt_from_fname: traffic_byte_sec}
        else:
            # tmp_stat[node][provider][tmp[node]["t_new"]] = traffic_byte_sec
            tmp_stat[node][provider][dt_from_fname] = traffic_byte_sec

        # the tmp dict contains every traffic entry for every provider on every node
        tmp[node][provider] = traffic_byte_total

    tmp[node]["t_old"] = tmp[node]["t_new"]

# save date and time from the last file to build
# the start and end datetime of all used files
last_dtime = dt_from_fname

# uncomment this to get every entry of every provider in every node
# attention: this is much data and creates big dumps
# print(json.dumps(tmp_stat, indent=2, sort_keys=True))


# --- calculate the avg traffic (bytes/sec) per provider per node

tmp['provider'] = {}

for node in tmp_stat:
    for provider in tmp_stat[node]:
        i = 0
        sum = 0.0

        for entry in tmp_stat[node][provider]:
            sum += tmp_stat[node][provider][entry]
            i += 1

        if node not in stat:
            stat[node] = {}
        stat[node][provider] = {}
        stat[node][provider]["avg_byte_per_sec"] = sum/i

        # calculate the avg traffic per provider over all nodes
        if provider not in tmp['provider']:
            tmp['provider'][provider] = {}
            tmp['provider'][provider] = {"avg_byte_per_sec": stat[node][provider]["avg_byte_per_sec"]}
        else:
            tmp['provider'][provider]["avg_byte_per_sec"] += stat[node][provider]["avg_byte_per_sec"]


# --- put the total avg traffic per provider over all nodes into the stat dict

stat['total'] = {}
for provider in tmp['provider']:
        stat['total'][provider] = {"avg_byte_per_sec": tmp['provider'][provider]['avg_byte_per_sec']}

# output of the total traffic
print("---------------------")
print("start " + first_dtime)
print("end   " + last_dtime)
print("---------------------")
print(json.dumps(stat, indent=2, sort_keys=True))
